﻿using Blizzer.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Blizzer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        LoginViewModel vm;
        ImageSource placeholder;

        public LoginPage()
        {
            InitializeComponent();
            BindingContext = vm = new LoginViewModel(Navigation);
            CircleImageAvatar.Source = placeholder = ImageSource.FromFile("profile_generic_big.png");
        }
    }
}