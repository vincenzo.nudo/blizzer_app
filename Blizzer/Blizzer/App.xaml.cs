﻿using Blizzer.Abstractions;
using Blizzer.Abstractions.Services.Localization;
using Blizzer.Core;
using Blizzer.Services.Localization;
using FormsToolkit;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Blizzer
{
    public partial class App : Application
    {
        protected IDialogService DialogService { get; private set; }
        private static NavigableElement navigationRoot;
        public static NavigableElement NavigationRoot
        {
            get => GetShellSection(navigationRoot) ?? navigationRoot;
            set => navigationRoot = value;
        }

        public static AppShell Shell => Current.MainPage as AppShell;

        static App()
        {
            BuildDependencies();
        }

        public App()
        {
            InitializeComponent();

            DialogService = Locator.Instance.Resolve<IDialogService>();
            ThemeManager.SetTheme(Device.RuntimePlatform == Device.Android);
        }

        public static void BuildDependencies()
        {
           //  Locator.Instance.Build();
        }

        internal static ShellSection GetShellSection(Element element)
        {
            if (element == null)
            {
                return null;
            }

            var parent = element;
            var parentSection = parent as ShellSection;

            while (parentSection == null && parent != null)
            {
                parent = parent.Parent;
                parentSection = parent as ShellSection;
            }

            return parentSection;
        }


        protected override void OnStart()
        {
            OnResume();
        }

        protected override void OnSleep()
        {

            if (!registered)
                return;

            registered = false;
            MessagingService.Current.Unsubscribe(MessageKeys.NavigateLogin);
            MessagingService.Current.Unsubscribe(MessageKeys.LoggedIn);
            MessagingService.Current.Unsubscribe<MessagingServiceQuestion>(MessageKeys.Question);
            MessagingService.Current.Unsubscribe<MessagingServiceAlert>(MessageKeys.Message);
            MessagingService.Current.Unsubscribe<MessagingServiceChoice>(MessageKeys.Choice);

            MessagingService.Current.Unsubscribe(MessageKeys.NavigateToNewTrip);
            MessagingService.Current.Unsubscribe(MessageKeys.NavigateToNewTripComleted);


            Connectivity.ConnectivityChanged -= Connectivity_ConnectivityChanged;
        }
        bool registered;
        bool firstRun = true;
        protected override void OnResume()
        {
            if (registered)
                return;

            registered = true;

            Settings.Current.IsConnected = Connectivity.NetworkAccess == NetworkAccess.Internet;
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;

            MessagingService.Current.Subscribe<MessagingServiceAlert>(MessageKeys.Message, async (m, info) =>
            {
                var task = Application.Current?.MainPage?.DisplayAlert(info.Title, info.Message, info.Cancel);

                if (task == null)
                    return;

                await task;
                info?.OnCompleted?.Invoke();
            });

            MessagingService.Current.Subscribe<MessagingServiceQuestion>(MessageKeys.Question, async (m, q) =>
            {
                var task = Application.Current?.MainPage?.DisplayAlert(q.Title, q.Question, q.Positive, q.Negative);
                if (task == null)
                    return;
                var result = await task;
                q?.OnCompleted?.Invoke(result);
            });

            MessagingService.Current.Subscribe<MessagingServiceChoice>(MessageKeys.Choice, async (m, q) =>
            {
                var task = Application.Current?.MainPage?.DisplayActionSheet(q.Title, q.Cancel, q.Destruction, q.Items);
                if (task == null)
                    return;
                var result = await task;
                q?.OnCompleted?.Invoke(result);
            });

            MessagingService.Current.Subscribe(MessageKeys.NavigateLogin, async m =>
            {
                Page page = new LoginPage();

                var nav = Application.Current?.MainPage?.Navigation;
                if (nav == null)
                    return;

                await NavigationService.PushModalAsync(nav, page);

            });

            MessagingService.Current.Subscribe(MessageKeys.LoggedIn, async m =>
            {
                Application.Current.MainPage = new AppShell();
                await Shell.GoToAsync("//chats");
            });

            //MessagingService.Current.Subscribe(MessageKeys.NavigateToNewTrip, async m => {
            //    var page = new NewTripPage();
            //    var nav = Application.Current?.MainPage?.Navigation;
            //    if (nav == null)
            //        return;
            //    await NavigationService.PushModalAsync(nav, page);
            //});

            MessagingService.Current.Subscribe(MessageKeys.NavigateToNewTripComleted, async m => {

                var nav = Application.Current?.MainPage?.Navigation;
                if (nav == null)
                    return;
                await nav.PopModalAsync();
            });


            try
            {
                if (Settings.Current.IsLoggedIn)
                    MainPage = new AppShell();
                else
                    MainPage = new LoginPage();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            var connected = Settings.Current.IsConnected;
            Settings.Current.IsConnected = e.NetworkAccess == NetworkAccess.Internet;
            // Was connected but now we are disconncted 
            if (connected && e.NetworkAccess != NetworkAccess.Internet)
            {
                await DialogService.ShowAlertAsync("Uh Oh, It looks like you have gone offline.", "Error", "Ok");
               
            }
        }
    }
}
