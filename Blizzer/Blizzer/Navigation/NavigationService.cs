﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace Blizzer
{
    public static class NavigationService
    {
        static bool navigating;
       
        public static async Task PushAsync(INavigation navigation, Page page, bool animate = true)
        {
            if (navigating)
                return;

            navigating = true;
            await navigation.PushAsync(page, animate);
            navigating = false;
        }
        
        public static async Task PushModalAsync(INavigation navigation, Page page, bool animate = true)
        {
            if (navigating)
                return;

            navigating = true;
            await navigation.PushModalAsync(page, animate);
            navigating = false;
        }
    }
    //public class NavigationService : INavigationService
    //{
    //    public Task InitializeAsync()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateBackAsync()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToAsync(Type viewModelType)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToAsync(Type viewModelType, object parameter)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToPopupAsync<TViewModel>(bool animate) where TViewModel : ViewModelBase
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task NavigateToPopupAsync<TViewModel>(object parameter, bool animate) where TViewModel : ViewModelBase
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public Task RemoveLastFromBackStackAsync()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
