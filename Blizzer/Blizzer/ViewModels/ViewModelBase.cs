﻿using Blizzer.Abstractions;
using Blizzer.Core;
using MvvmHelpers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Blizzer.ViewModels
{
    public class ViewModelBase : BaseViewModel
    {
        protected IBlizzerLogger Logger { get; private set; } 
        protected IDialogService DialogService { get; private set; }
        protected INavigation Navigation { get; }

        private string _pageTitle = string.Empty;

        public string PageTitle
        {
            get => _pageTitle;
            set => SetProperty(ref _pageTitle, value);
        }

        public ViewModelBase(INavigation navigation = null)
        {
            Logger = Locator.Instance.Resolve<IBlizzerLogger>();
            DialogService = Locator.Instance.Resolve<IDialogService>();

            Navigation = navigation;

        }

        public virtual Task InitializeAsync(object navigationData) => Task.FromResult(false);

    }
}
