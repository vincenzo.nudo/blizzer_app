﻿using Blizzer.Abstractions;
using Blizzer.Core;
using FormsToolkit;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Blizzer.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        IAuthenticationService _authenticationService;
        string userName;
        string password;

        public LoginViewModel(INavigation navigation) : base(navigation)
        {
            _authenticationService = Locator.Instance.Resolve<IAuthenticationService>();
        }

        string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        public string UserName
        {
            get => userName;
            set => SetProperty(ref userName, value);
        }

        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }


        public ICommand LoginCommand
        {
            get
            {
                if (loginCommand == null)
                {
                    loginCommand = new Command(async () => await ExecuteLoginAsync());
                }
                return loginCommand;
            }
        }
        ICommand loginCommand;

        async Task ExecuteLoginAsync()
        {
            if (IsBusy)
                return;

            if (string.IsNullOrWhiteSpace(userName))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your email address",
                    Cancel = "OK"
                });
                return;
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "Password is empty!",
                    Cancel = "OK"
                });
                return;
            }

            try
            {
                IsBusy = true;
                Message = "Signing in...";

                TokenResponse result = null;

                result = await _authenticationService.LoginAsync(UserName, Password);

                if (result?.Success ?? false)
                {
                    Settings.Current.TokenResponse = result;
                    Message = "Updating trips...";
                    Settings.Current.FirstName = result.FirstName;
                    Settings.Current.LastName = result.LastName;
                    Settings.Current.Email = result.Email.ToLowerInvariant();
                    Logger.Track(ConferenceLoggerKeys.LoginSuccess);

                    MessagingService.Current.SendMessage(MessageKeys.LoggedIn);
                    Settings.Current.FirstRun = false;

                }
                else
                {
                    Logger.Track(ConferenceLoggerKeys.LoginFailure, "Reason", result.Message);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sign in",
                        Message = "Login fail",
                        Cancel = "OK"
                    });
                }

            }
            catch (Exception ex)
            {
                Logger.Track(ConferenceLoggerKeys.LoginFailure, "Reason", ex.ToString());
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
        }

        //ICommand cancelCommand;
        //public ICommand CancelCommand =>
        //    cancelCommand ?? (cancelCommand = new Command(async () => await ExecuteCancelAsync()));

        //async Task ExecuteCancelAsync()
        //{
        //    Logger.Track(ConferenceLoggerKeys.LoginCancel);
        //    if (Settings.Current.FirstRun)
        //    {
        //        try
        //        {
        //            Message = "Updating trips...";
        //            IsBusy = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Report(ex);
        //        }
        //        finally
        //        {
        //            Message = string.Empty;
        //            IsBusy = false;
        //        }
        //    }
        //    // await Finish();
        //    Settings.Current.FirstRun = false;
        //}
        ////async Task Finish()
        ////{
        ////    await Navigation.PopModalAsync();
        ////}
    }
}
