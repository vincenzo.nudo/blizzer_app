﻿using Blizzer.Resources;

namespace Blizzer.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {

        public SettingsViewModel()
        {
            PageTitle = AppResources.Settings;
        }
    }
}
