﻿using Blizzer.Abstractions.Services.Localization;
using Blizzer.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Blizzer.Utilities
{
    [ContentProperty("Id")]
    public class I18nExtension : IMarkupExtension
    {
        private II18nService _i18nService;

        public I18nExtension()
        {
            _i18nService = Locator.Instance.Resolve<II18nService>();
        }

        public string Id { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
        public string P3 { get; set; }
        public bool Header { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            var val = _i18nService.T(Id, P1, P2, P3);
            /*
            if(Header && Device.RuntimePlatform == Device.iOS)
            {
                return val.ToUpper();
            }
            */
            return val;
        }
    }
}
