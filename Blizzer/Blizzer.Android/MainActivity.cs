﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using ImageCircle.Forms.Plugin.Droid;
using Acr.UserDialogs;

namespace Blizzer.Droid
{
    [Activity(Label = "Blizzer", 
        Icon = "@mipmap/icon", 
        Theme = "@style/LightTheme.Splash", 
        MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            UserDialogs.Init(this);
            //CachedImageRenderer.Init(true);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: true);
            ImageCircleRenderer.Init();

            UpdateTheme(ThemeManager.GetTheme(true));

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.SetFlags("Shell_Experimental", "Visual_Experimental", "CollectionView_Experimental", "FastRenderers_Experimental");
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void UpdateTheme(string theme)
        {
            if (theme == "dark")
            {
                SetTheme(Resource.Style.DarkTheme);
            }
            else if (theme == "black")
            {
                SetTheme(Resource.Style.BlackTheme);
            }
            else if (theme == "nord")
            {
                SetTheme(Resource.Style.NordTheme);
            }
            else
            {
                SetTheme(Resource.Style.LightTheme);
            }
        }
    }
}