﻿using Android.App;
using Android.Content;
using Android.Gms.Security;
using Android.Runtime;
using Autofac;
using Blizzer.Abstractions.Services.Localization;
using Blizzer.Core;
using Blizzer.Droid.Services;
using Blizzer.Services.Localization;
using Plugin.CurrentActivity;
using System;

namespace Blizzer.Droid
{
#if DEBUG
    [Application(Debuggable = true)]
#else
    [Application(Debuggable = false)]
#endif

    [Register("com.naked.blizzer.MainApplication")]
    public class MainApplication : Application, ProviderInstaller.IProviderInstallListener
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
          : base(handle, transer)
        {
            RegisterLocalServices();
            Locator.Instance.Build();
        }
        public override void OnCreate()
        {
            base.OnCreate();
            Bootstrap();
            CrossCurrentActivity.Current.Init(this);
        }

        private void Bootstrap()
        {
            (Locator.Instance.Resolve<II18nService>() as MobileI18nService).Init();
        }

        private void RegisterLocalServices()
        {
            var localizeService = new LocalizeService();
            var i18nService = new MobileI18nService(localizeService.GetCurrentCultureInfo());
            Locator.Instance.ContainerBuilder.RegisterInstance(i18nService).As<II18nService>(); 
        }

        public void OnProviderInstallFailed(int errorCode, Intent recoveryIntent)
        {
        }

        public void OnProviderInstalled()
        {
        }
    }
}