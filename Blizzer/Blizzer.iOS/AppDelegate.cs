﻿using Autofac;
using Blizzer.Abstractions.Services.Localization;
using Blizzer.Core;
using Blizzer.iOS.Helpers;
using Blizzer.iOS.Services;
using Blizzer.Services.Localization;
using Foundation;
using UIKit;

namespace Blizzer.iOS
{

    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
            

            global::Xamarin.Forms.Forms.SetFlags("Shell_Experimental", "Visual_Experimental", "CollectionView_Experimental", "FastRenderers_Experimental");

            global::Xamarin.Forms.Forms.Init();
            InitApp();

            LoadApplication(new App());

            ThemeHelpers.SetAppearance(ThemeManager.GetTheme(false));
            UIApplication.SharedApplication.StatusBarHidden = false;
            UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;

            return base.FinishedLaunching(app, options);
        }

        public void InitApp()
        {
            var localizeService = new LocalizeService();
            var i18nService = new MobileI18nService(localizeService.GetCurrentCultureInfo());
            Locator.Instance.ContainerBuilder.RegisterInstance(i18nService).As<II18nService>();

            Locator.Instance.Build();
            Bootstrap();
        }
        private void Bootstrap()
        {
            (Locator.Instance.Resolve<II18nService>() as MobileI18nService).Init();
        }
    }
}
