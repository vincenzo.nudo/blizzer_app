﻿using System.Globalization;

namespace Blizzer.Abstractions.Services.Localization
{
    public interface ILocalizeService
    {
        CultureInfo GetCurrentCultureInfo();
    }
}
