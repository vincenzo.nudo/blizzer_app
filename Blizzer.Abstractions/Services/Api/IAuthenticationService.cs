﻿using System.Threading.Tasks;

namespace Blizzer.Abstractions
{
    public interface IAuthenticationService
    {
        bool IsAuthenticated { get; }

        UserDto AuthenticatedUser { get; }

        Task<TokenResponse> LoginAsync(string email, string password);

        Task<bool> UserIsAuthenticatedAndValidAsync();

        Task LogoutAsync();
    }
}
