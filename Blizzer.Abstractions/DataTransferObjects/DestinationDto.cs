﻿using System;
using System.Collections.Generic;

namespace Blizzer.Abstractions
{
    public class DestinationDto
    {
        public Int64 Id { get; set; }
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string DisplayType { get; set; }
    }

    public class DestinationResult
    {
        public bool Success { get; set; }
        public List<DestinationDto> Destinations { get; set; }
    }
}
