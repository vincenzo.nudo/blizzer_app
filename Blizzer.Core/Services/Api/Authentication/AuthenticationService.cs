﻿using Blizzer.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Blizzer.Core
{
    public class AuthenticationService : IAuthenticationService
    {
         
        readonly HttpClient client;

        public bool IsAuthenticated
        {
            get
            {
                return false;
            }
        }

        public UserDto AuthenticatedUser
        {
            get
            {
                return null;
            }
        }

        public AuthenticationService( )
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(Settings.Current.TokenEndpoint)
            };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
            
        }

        async Task<string> PostForm(string endpoint, IDictionary<string, string> keyValues)
        {
            var response = await client.PostAsync(endpoint, new FormUrlEncodedContent(keyValues));
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<TokenResponse> LoginAsync(string email, string password)
        {
            TokenResponse response = null;
            try
            {
                var json = await PostForm("token", new Dictionary<string, string>
                {
                    {"username", email},
                    {"password", password},
                    {"grant_type", "password"},
                });
                response = JsonConvert.DeserializeObject<TokenResponse>(json);
            }
            catch (Exception ex)
            {
                if (response == null)
                    response = new TokenResponse();

                response.Message = ex.ToString();
            }
            return response;
        }

        public Task LogoutAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserIsAuthenticatedAndValidAsync()
        {
            throw new NotImplementedException();
        }
    }
}
