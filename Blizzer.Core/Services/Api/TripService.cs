﻿using Blizzer.Abstractions;
using Blizzer.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blizzer.Core
{
    //public class TripService : ITripService
    //{
    //    readonly IRequestService RequestService;
    //    public TripService(IRequestService requestService)
    //    {
    //        RequestService = requestService;
    //    }

    //    public async Task CreateAsync(TripsDto trip)
    //    {
    //        await RequestService.PostAsync<TripsDto>(Settings.Current.TokenEndpoint, trip, Settings.Current.TokenResponse.AccessToken);
    //    }

    //    public async Task DeleteAsync(Guid tripId)
    //    {
    //        var builder = new UriBuilder(Settings.Current.TripEndpoint);
    //        builder.AppendToPath($"{tripId}");
    //        var uri = builder.ToString();
    //        await RequestService.DeleteAsync<string>(uri, Settings.Current.TokenResponse.AccessToken);
    //    }

    //    public async Task<TripsDto> GetAsync(Guid tripId)
    //    {
    //        var builder = new UriBuilder(Settings.Current.TripEndpoint);
    //        builder.AppendToPath($"{tripId}");
    //        var uri = builder.ToString();
    //        return await RequestService.GetAsync<TripsDto>(uri, Settings.Current.TokenResponse.AccessToken);
    //    }

    //    public async Task<TripsResult> GetAllAsync()
    //    {
    //        var builder = new UriBuilder(Settings.Current.TripEndpoint);
    //        var uri = builder.ToString();
    //        return await RequestService.GetAsync<TripsResult>(uri, Settings.Current.TokenResponse.AccessToken);
    //    }

    //    public async  Task UpdateAsync(TripsDto trip)
    //    {
    //        await RequestService.PutAsync<TripsDto>(Settings.Current.TokenEndpoint, trip, Settings.Current.TokenResponse.AccessToken);
    //    }

    //    public async Task<DestinationResult> GetDestionationsAsync(string search)
    //    {
    //        var builder = new UriBuilder(Settings.Current.TripEndpoint);
    //        builder.AppendToPath($"Destination/{search}");
    //        var uri = builder.ToString();
    //        return await RequestService.GetAsync<DestinationResult>(uri,Settings.Current.TokenResponse.AccessToken);
    //    }

    //}
}
