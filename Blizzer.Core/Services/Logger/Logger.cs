﻿using Blizzer.Abstractions;
using System;
using System.Collections;
using System.Diagnostics;

namespace Blizzer.Core
{
    public class BlizzerLogger: IBlizzerLogger
    {
        public virtual void TrackPage(string page, string id = null)
        {
            Debug.WriteLine("BlizzerTrips Logger: TrackPage: " + page.ToString() + " Id: " + id ?? string.Empty);
        }

        public virtual void Track(string trackIdentifier)
        {
            Debug.WriteLine("BlizzerTrips Logger: Track: " + trackIdentifier);
        }

        public virtual void Track(string trackIdentifier, string key, string value)
        {
            Debug.WriteLine("BlizzerTrips Logger: Track: " + trackIdentifier + " key: " + key + " value: " + value);
        }

        public virtual void Report(Exception exception = null, Severity warningLevel = Severity.Warning)
        {
            Debug.WriteLine("BlizzerTrips Logger: Report: " + exception);
        }
        public virtual void Report(Exception exception, IDictionary extraData, Severity warningLevel = Severity.Warning)
        {
            Debug.WriteLine("BlizzerTrips Logger: Report: " + exception);
        }
        public virtual void Report(Exception exception, string key, string value, Severity warningLevel = Severity.Warning)
        {
            Debug.WriteLine("BlizzerTrips Logger: Report: " + exception + " key: " + key + " value: " + value);
        }
    }
}
