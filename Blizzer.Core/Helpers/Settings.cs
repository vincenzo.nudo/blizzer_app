﻿using Blizzer.Abstractions;
using Blizzer.Core.Utils;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Essentials;

namespace Blizzer.Core
{
    public class Settings : INotifyPropertyChanged
    {
        static Settings settings;

        public static Settings Current => settings ?? (settings = new Settings());

        static string defaultTokenEndpoint;

        static string defaultTripsEndpoint;


        public string UserDisplayName => IsLoggedIn ? $"{FirstName} {LastName}" : "Sign In";

        public string UserAvatar => IsLoggedIn  ? " " : "profile_generic.png";

        public bool IsLoggedIn => !string.IsNullOrWhiteSpace(Email);

        static Settings()
        {
            defaultTokenEndpoint = "http://192.168.1.12:49423/api/";
            defaultTripsEndpoint = "http://192.168.1.12:49423/api/trips";
        }

        bool isConnected;
        public bool IsConnected
        {
            get { return isConnected; }
            set
            {
                if (isConnected == value)
                    return;
                isConnected = value;
                OnPropertyChanged();
            }
        }

        const string FirstRunKey = "first_run";
        static readonly bool FirstRunDefault = true;

        public TokenResponse TokenResponse
        {
            get => PreferencesHelpers.Get(nameof(TokenResponse), default(TokenResponse));
            set => PreferencesHelpers.Set(nameof(TokenResponse), value);
        }
        public string Token
        {
            get => Preferences.Get(nameof(TokenEndpoint), defaultTokenEndpoint);
            set => Preferences.Set(nameof(TokenEndpoint), value);
        }

        public string TokenEndpoint
        {
            get => Preferences.Get(nameof(TokenEndpoint), defaultTokenEndpoint);
            set => Preferences.Set(nameof(TokenEndpoint), value);
        }
        public string TripEndpoint
        {
            get => Preferences.Get(nameof(TokenEndpoint), defaultTripsEndpoint);
            set => Preferences.Set(nameof(TokenEndpoint), value);
        }
        public bool FirstRun
        {
            get => Preferences.Get(FirstRunKey, FirstRunDefault);
            set { Preferences.Set(FirstRunKey, value); OnPropertyChanged(); }
        }

        const string EmailKey = "email_key";
        readonly string EmailDefault = string.Empty;
        public string Email
        {
            get => Preferences.Get(EmailKey, EmailDefault);
            set
            {
                Preferences.Set(EmailKey, value);
                OnPropertyChanged();
            }
        }

        const string FirstNameKey = "firstname_key";
        readonly string FirstNameDefault = string.Empty;
        public string FirstName
        {
            get => Preferences.Get(FirstNameKey, FirstNameDefault);
            set
            {
                Preferences.Set(FirstNameKey, value);
                OnPropertyChanged();
                OnPropertyChanged(nameof(UserDisplayName));
            }
        }
        const string LastNameKey = "lastname_key";
        readonly string LastNameDefault = string.Empty;
        public string LastName
        {
            get => Preferences.Get(LastNameKey, LastNameDefault);
            set
            {
                Preferences.Set(LastNameKey, value);
                OnPropertyChanged();
                OnPropertyChanged(nameof(UserDisplayName));
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName]string name = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        #endregion

    }
}
