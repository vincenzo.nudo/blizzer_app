﻿namespace Blizzer.Core
{
    public static class MessageKeys
    {
        public const string NavigateToNewTrip = "navigate_new_trip";
        public const string NavigateToNewTripComleted = "navigate_new_trip_completed";
        public const string NavigateToTrip = "navigate_trip";
        public const string NavigateLogin = "navigate_login";
        public const string Error = "error";
        public const string Connection = "connection";
        public const string LoggedIn = "loggedin";
        public const string Message = "message";
        public const string Question = "question";
        public const string Choice = "choice";
    }
}
