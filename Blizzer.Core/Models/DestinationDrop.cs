﻿using SupportWidgetXF.Models.Widgets;
using System;

namespace Blizzer.Core.Models
{
    public class DestinationDrop : IAutoDropItem
    {
        public string Title { set; get; }
        public string Description { set; get; }
        public string Icon { set; get; }
        public bool Checked { set; get; }
        public DestinationDrop(string title, string description, string icon)
        {
            Title = title;
            Description = description;
            Icon = icon;
        }
        public Action IF_GetAction()
        {
            return null;
        }

        public string IF_GetDescription()
        {
            return Description;
        }

        public string IF_GetIcon()
        {
            return Icon;
        }

        public string IF_GetTitle()
        {
            return Title;
        }

        public void IF_SetChecked(bool _Checked)
        {
            Checked = _Checked;
        }
        public bool IF_GetChecked()
        {
            return Checked;
        }
    }
}
