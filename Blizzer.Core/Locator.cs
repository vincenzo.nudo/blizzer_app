﻿using Autofac;
using Blizzer.Abstractions;
using System;

namespace Blizzer.Core
{
    public class Locator
    {
        IContainer container;
        public ContainerBuilder ContainerBuilder;

        public static Locator Instance { get; } = new Locator();

        public Locator()
        {
            ContainerBuilder = new ContainerBuilder();
            // Framework
            //containerBuilder.RegisterType<NavigationService>().As<INavigationService>();

            ContainerBuilder.RegisterType<DialogService>().As<IDialogService>();
            ContainerBuilder.RegisterType<BlizzerLogger>().As<IBlizzerLogger>();
             

            // API

            ContainerBuilder.RegisterType<RequestService>().As<IRequestService>();
            ContainerBuilder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            
        }

        public T Resolve<T>() => container.Resolve<T>();

        public object Resolve(Type type) => container.Resolve(type);

        public void Register<TInterface, TImplementation>() where TImplementation : TInterface => ContainerBuilder.RegisterType<TImplementation>().As<TInterface>();

        public void Register<T>() where T : class => ContainerBuilder.RegisterType<T>();

        public void Build() => container = ContainerBuilder.Build();
    }
}
